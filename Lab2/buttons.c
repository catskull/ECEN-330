/*
 * buttons.c
 *
 *  Created on: Sep 10, 2015
 *      Author: dwdegraw
 */

#include "buttons.h"
#include "xparameters.h"
#include "supportFiles/mio.h"
#include "supportFiles/display.h"

// Were dealing with gpio channel 1 for buttons
#define BUTTONS_GPIO_CHANNEL_NUMBER 1
// Output is 0000
#define BUTTONS_GPIO_DIRECTION_IS_OUTPUT 0x0
// Input is 1111
#define BUTTONS_GPIO_DIRECTION_IS_INPUT 0xF
// Tri state offset is BASE_ADDRESS + 4
#define BUTTONS_TRI_STATE_OFFSET 0X04
// GPIO base address
#define BUTTONS_GPIO_DEVICE_BASE_ADDRESS XPAR_PUSH_BUTTONS_BASEADDR
// Tri state address
#define BUTTONS_TRI_STATE_DEVICE_ADDRESS BUTTONS_GPIO_DEVICE_BASE_ADDRESS + BUTTONS_TRI_STATE_OFFSET
// Bitmask for all

// A constant for button 1
#define BUTTONS_BUTTON1 0x01
// A constant for button 2
#define BUTTONS_BUTTON2 0x02
// A constant for button 3
#define BUTTONS_BUTTON3 0x04
// A constant for button 4
#define BUTTONS_BUTTON4 0x08
// Bitmask to check if all buttons are pressed
#define BUTTONS_ARE_ALL_PRESSED 0xF

// The size of the text to display on screen
#define BUTTONS_TEXT_SIZE 2

// How big each button's rectangle should be
// They are all the same size, so use the same values
#define BUTTONS_ALL_BUTTONS_X_LENGTH 80
#define BUTTONS_ALL_BUTTONS_Y_LENGTH 120

// All button's text is on the same line, so share the value
#define BUTTONS_ALL_BUTTONS_CURSOR_Y_CORD 55

// Button 1's on screen rectangle values
#define BUTTONS_BUTTON1_X_CORD 239
#define BUTTONS_BUTTON1_CURSOR_X_CORD 256
#define BUTTONS_BUTTON1_TEXT "BTN0"

// Button 2's on screen rectangle values
#define BUTTONS_BUTTON2_X_CORD 159
#define BUTTONS_BUTTON2_CURSOR_X_CORD 176
#define BUTTONS_BUTTON2_TEXT "BTN1"

// Button 3's on screen rectangle values
#define BUTTONS_BUTTON3_X_CORD 79
#define BUTTONS_BUTTON3_CURSOR_X_CORD 96
#define BUTTONS_BUTTON3_TEXT "BTN2"

// Button 4's on screen rectangle values
#define BUTTONS_BUTTON4_CURSOR_X_CORD 16
#define BUTTONS_BUTTON4_TEXT "BTN3"

// The total width of the screen
#define BUTTONS_SCREEN_WIDTH 320


// This will init the GPIO hardware so you can write to the 4 LEDs  (LED3 - LED0) on the ZYBO board.
int buttons_init() {
    // set up a pointer to the tri state address
    uint32_t *ptr = (uint32_t *) BUTTONS_TRI_STATE_DEVICE_ADDRESS;
    // set the value at the pointer to input
    *ptr = BUTTONS_GPIO_DIRECTION_IS_INPUT;
    // return OK
    return BUTTONS_INIT_STATUS_OK;
}

int32_t buttons_read() {
    // Note that you have to include a cast (uint32_t *) to keep the compiler happy.
    uint32_t *ptr = (uint32_t *) BUTTONS_GPIO_DEVICE_BASE_ADDRESS;
    return *ptr;
}

void buttons_runTest() {
    buttons_init();
    // Initialize the display for writing.
    display_init();
    // Clear the screen; make the whole thing black.
    display_fillScreen(DISPLAY_BLACK);
    // Set up a var for storing the current value of the buttons.
    // This will be updated every time our while loop cycles.
    uint32_t newVal = 0;
    // Set up a var for storing the previous value of the buttons.
    // This will be updated every time our while loop cycles.
    uint32_t lastVal = 0;
    // Set the text size to a value of 2.
    display_setTextSize(BUTTONS_TEXT_SIZE);

    // As long as all four buttons are not all pressed at once, do this:
    while(newVal ^ BUTTONS_ARE_ALL_PRESSED){
        // Copy newVal into lastVal because we're going to update newVal.
        lastVal = newVal;
        // Read the state of the buttons into newVal.
        newVal = buttons_read();
        // We need to check whether BUTTONS_BUTTON1's corresponding square has
        // already been drawn. We don't want to draw it again if it's
        // already there.
        if(!(lastVal & BUTTONS_BUTTON1) && (newVal & BUTTONS_BUTTON1)){
            // Fill the far right square with yellow.
            display_fillRect(BUTTONS_BUTTON1_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_YELLOW);
            // Set our text cursor to a good place in the center of the square.
            display_setCursor(BUTTONS_BUTTON1_CURSOR_X_CORD, BUTTONS_ALL_BUTTONS_CURSOR_Y_CORD);
            // Make the text color black.
            display_setTextColor(DISPLAY_BLACK);
            // Write BTN0 in the center of the square.
            display_println(BUTTONS_BUTTON1_TEXT);
        }
        // If the square is drawn, and button 1 is no longer pressed:
        else if((lastVal & BUTTONS_BUTTON1) && !(newVal & BUTTONS_BUTTON1)){
            // Write a black square on top of the yellow square.
            display_fillRect(BUTTONS_BUTTON1_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLACK);
        }
        // Repeat the process for BUTTONS_BUTTON1 for all other buttons:
        if(!(lastVal & BUTTONS_BUTTON2) && (newVal & BUTTONS_BUTTON2)){
            // Draw a green square
            display_fillRect(BUTTONS_BUTTON2_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_GREEN);
            // Set the cursor
            display_setCursor(BUTTONS_BUTTON2_CURSOR_X_CORD, BUTTONS_ALL_BUTTONS_CURSOR_Y_CORD);
            // Set text color to black
            display_setTextColor(DISPLAY_BLACK);
            // Write BTN1
            display_println(BUTTONS_BUTTON2_TEXT);
        }
        // If the BUTTONS_BUTTON2 square is there and BUTTONS_BUTTON2 is no longer pressed:
        else if((lastVal & BUTTONS_BUTTON2) && !(newVal & BUTTONS_BUTTON2)){
            // Draw a black square over the top of the green one
            display_fillRect(BUTTONS_BUTTON2_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLACK);
        }
        // If BUTTONS_BUTTON3 is pressed and the square is not already drawn:
        if(!(lastVal & BUTTONS_BUTTON3) && (newVal & BUTTONS_BUTTON3)){
            // Draw a red rectangle
            display_fillRect(BUTTONS_BUTTON3_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_RED);
            // Set the cursor
            display_setCursor(BUTTONS_BUTTON3_CURSOR_X_CORD, BUTTONS_ALL_BUTTONS_CURSOR_Y_CORD);
            // Set the text color to white
            display_setTextColor(DISPLAY_WHITE);
            // Write BTN2 to the screen
            display_println(BUTTONS_BUTTON3_TEXT);
        }
        // If BUTTONS_BUTTON3 is no longer pressed and the square is on the screen:
        else if((lastVal & BUTTONS_BUTTON3) && !(newVal & BUTTONS_BUTTON3)){
            // Draw a black rectangle over the top of the red one
            display_fillRect(BUTTONS_BUTTON3_X_CORD, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLACK);
        }
        // If BUTTONS_BUTTON4 is pressed and the square isn't on the screen:
        if(!(lastVal & BUTTONS_BUTTON4) && (newVal & BUTTONS_BUTTON4)){
            // Draw a blue rectangle
            display_fillRect(0, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLUE);
            // Set the cursor
            display_setCursor(BUTTONS_BUTTON4_CURSOR_X_CORD, BUTTONS_ALL_BUTTONS_CURSOR_Y_CORD);
            // Set the text to be white
            display_setTextColor(DISPLAY_WHITE);
            // Write BTN3 to the screen
            display_println(BUTTONS_BUTTON4_TEXT);
        }
        // If BUTTONS_BUTTON4 is no longer pressed and the square is on the screen:
        else if((lastVal & BUTTONS_BUTTON4) && !(newVal & BUTTONS_BUTTON4)){
            // Draw a black square over the blue one
            display_fillRect(0, 0, BUTTONS_ALL_BUTTONS_X_LENGTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLACK);
        }
    }
    // Once all 4 buttons are pressed simultaneously,
    // Draw a black square over the top part of the screen
    // To reset it.
    display_fillRect(0, 0, BUTTONS_SCREEN_WIDTH, BUTTONS_ALL_BUTTONS_Y_LENGTH, DISPLAY_BLACK);
}
