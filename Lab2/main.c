/*
 * main.c
 *
 *  Created on: Sep 10, 2015
 *      Author: dwdegraw
 */
// Include the buttons and switches header files
#include "buttons.h"
#include "switches.h"

int main() {
    // run the buttons test first.
    // When all four buttons are pressed, exit the test.
    buttons_runTest();
    // Run the switches test.
    // When all four switches are on (up), turn them off and we're done!
    switches_runTest();
}

void isr_function() {
    // Empty
}

