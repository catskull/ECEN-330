/*
 * switches.c
 *
 *  Created on: Sep 10, 2015
 *      Author: dwdegraw
 */

#include "switches.h"
#include "xgpio.h"
#include "xil_io.h"
#include "supportFiles/leds.h"

// The offset from the switch base address that contains the tri state
#define SWITCHES_TRI_STATE_OFFSET 0x4
// the address of the tri state
#define SWITCHES_TRI_STATE_ADDRESS XPAR_SLIDE_SWITCHES_BASEADDR + SWITCHES_TRI_STATE_OFFSET
// a value to set all 4 bits true. Used for setting switches.
#define SWITCHES_INPUT 0xF
// bitmask for all switches on
#define SWITCHES_ARE_ALL_ON 0xF

// This function will set the tri state driver for the switches to be all 1's
// for input
int switches_init(){
    // Set the switches to be inputs
    Xil_Out32(SWITCHES_TRI_STATE_ADDRESS, SWITCHES_INPUT);
    // Initialize the LEDs
    leds_init(0);
    // Return OK
    return SWITCHES_INIT_STATUS_OK;
}

// This function will read the switches and return a value
// The 4 least significant bits contain the switch values
int32_t switches_read(){
    // Return the switches state
    return Xil_In32(XPAR_SLIDE_SWITCHES_BASEADDR);
}

// This function simply reads the switches
// And sets the corresponding led's to be on
// If all 4 switches are on, it turns off leds
// and exits.
void switches_runTest(){
    // Initialize the switches
    switches_init();
    // Set up a var to store the buttons
    uint32_t buttons = 0;
    // While all buttons are not pressed
    while(buttons ^ SWITCHES_ARE_ALL_ON){
        // Read buttons and update our variable
        buttons = switches_read();
        // Write the button value to the leds
        leds_write(buttons & SWITCHES_ARE_ALL_ON);
    }
    // Turn all LEDs off
    leds_write(!buttons);
}
