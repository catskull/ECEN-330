/*
 * clockControl.h
 *
 *  Created on: Oct 1, 2015
 *      Author: dwdegraw
 */

#ifndef CLOCKCONTROL_H_
#define CLOCKCONTROL_H_

// This function will tick the state machine once.
// It should be called once per period.
// This function has been coded with the prerequesit that the period be set to exactly 100ms.
void clockControl_tick();


#endif /* CLOCKCONTROL_H_ */
