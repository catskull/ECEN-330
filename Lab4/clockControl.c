/*
 * clockControl.c
 *
 *  Created on: Oct 1, 2015
 *      Author: dwdegraw
 */

#include "clockControl.h"
#include "supportFiles/display.h"
#include "clockDisplay.h"
#include <stdio.h>

// The period ended up being ~100ms. This was chosen because any values lower would result in
// missed interrupts. It actually could have been ~91ms, but for the sake of ease while waiting for
// only .5s before auto incrementing, it was rounded up to 100ms.
// ADD_EXPIRED is the time it takes for the ADC to settle after receiving touch input.
// It really only needs to be ~50ms, but since our period is 100ms I have to set it to 1.
// If my code was further optimized, my period could be ~50ms.
#define ADD_EXPIRED 1
// AUTO_EXPIRED is how many cycles an arrow must be held before the clock will begin
// quickly increasing/decreasing. It should be ~.5 seconds. Since the period is 100ms,
// make it 5 so it waits for 5 cycles or 500ms.
#define AUTO_EXPIRED 5
// RATE_EXPIRED is how may cycles it takes for a number to increase while
// the button is being held down. We want the numbers to increase roughly
// 10x the normal clock rate. Since our period is 100ms, set it to one.
#define RATE_EXPIRED 1
// TOUCH_EXPIRED allows me to keep time accuratly. Since our period is 100ms,
// I only want to add a second once every ten cycles.
#define TOUCH_EXPIRED 10

// This contains all the states possible.
enum CC_states { CC_init, CC_waiting_for_touch, CC_add_timer, CC_auto_timer, CC_rate_timer, CC_rate_timer_expired  } CC_state;

// A number to increment while we wait for ADC to settle.
int8_t addTimer = 0;
// A number to increment while we wait for ~.5 seconds that the button
// is being held down before we start quickly adding the clock.
int16_t autoTimer = 0;
// A number to increment while we wait before adding to the number field.
int16_t rateTimer = 0;

// A simple flag to detrmine whether the display has been touched at least once.
// 0 means it has not been touched, 1 means it has been touched.
int8_t touched = 0;

// A number to increment while we wait to add another second if the clock
// is incrementing as a normal clock. Add one second per second.
int8_t soak = 0;

// This is a debug state print routine. It will print the names of the states each
// time tick() is called. It only prints states if they are different than the
// previous state.
// This code was taken and modified from the code posted on the wiki for debugging state machines.
void debugStatePrint() {
  static CC_states previousState;
  static bool firstPass = true;
  // Only print the message if:
  // 1. This the first pass and the value for previousState is unknown.
  // 2. previousState != currentState - this prevents reprinting the same state name over and over.
  if (previousState != CC_state || firstPass) {
    firstPass = false;                // previousState will be defined, firstPass is false.
    previousState = CC_state;     // keep track of the last state that you were in.
    switch(CC_state) {            // This prints messages based upon the state that you were in.
      case CC_init:
        printf("init_st\n\r");
        break;
      case CC_waiting_for_touch:
        printf("waiting_for_touch_st\n\r");
        break;
      case CC_add_timer:
        printf("ad_timer_running_st\n\r");
        break;
      case CC_auto_timer:
        printf("auto_timer_running_st\n\r");
        break;
      case CC_rate_timer:
        printf("rate_timer_running_st\n\r");
        break;
      case CC_rate_timer_expired:
        printf("rate_timer_expired_st\n\r");
        break;
     }
  }
}

// This function will cause our state machine to "tick".
// Once per period this function will be called.
void clockControl_tick(){
    // print out the state we are in
    debugStatePrint();

    // This switch statment checks the current state
    // and sets the mealy outputs
    switch (CC_state){
        // CC_init state doesnt do anything.
        // It is here because the text recommends having an init state.
        case CC_init:
            // empty
            break;

        // This state will allow the clock to run at normal speed as
        // long as the display has been touched at least once.
        case CC_waiting_for_touch:
            // Reset all the timers
            addTimer = 0;
            autoTimer = 0;
            rateTimer = 0;
            // if the display has been touched at least once:
            if (touched){
                // if we've waited long enough before adding another second:
                if (soak == TOUCH_EXPIRED){
                    // increment time by one second
                    clockDisplay_advanceTimeOneSecond();
                    // update the display with the new time
                    clockDisplay_updateTimeDisplay(0);
                    // reset the counter
                    soak = 0;
                }
                // Otherwise, add one to the counter
                else {
                    soak++;
                }
            }
            break;

        // This state is where we wait for the ADC to settle.
        case CC_add_timer:
            //printf("add timer state\n");
            // If display has never been touched before, set the flag
            // indicating it has now been touched and allow the clock
            // to tick normally.
            if (!touched){
              touched = 1;
            }
            // Increment the counter
            addTimer++;
            break;

        // This state is where we wait for a delay before we start
        // quickly adding time to the clock.
        case CC_auto_timer:
            //printf("auto timer state\n");
            // Increment the counter
            autoTimer++;
            break;

        // This state is where we wait for a delay while we quickly increment
        // the clock. The goal is that while the buttin is being held, we increment
        // 10 times a second.
        case CC_rate_timer:
            //printf("rate timer state\n");
            // Increment the timer.
            rateTimer++;
            break;

        // This state is where we actually add a unit to the field being held.
        // For our transition, all we have to do is reset rateTimer
        case CC_rate_timer_expired:
            //printf("rate timer expired state\n");
            // reset rateTimer
            rateTimer = 0;
            break;
    }


    // This switch statement performs the transitions.
    switch (CC_state){
        // For init state, do nothing. Go straight to waiting for touch state.
        case CC_init:
            // set the state to wait for touch
            CC_state = CC_waiting_for_touch;
            break;

        // This state waits for the display to be touched.
        case CC_waiting_for_touch:
            // if the display is currently touched:
            if (display_isTouched()){
                // clear old touch data
                display_clearOldTouchData();
                // and go to the state to wait for the ADC to settle
                CC_state = CC_add_timer;
            }
            break;

        // This state simply waits for either the display to be un touched
        // or for the ADC to settle.
        case CC_add_timer:
            // if the display is still touched by the time the ADC's have settled:
            if (display_isTouched() && addTimer == ADD_EXPIRED){
                // go to the next state
                CC_state = CC_auto_timer;
            }
            // if the display is no longer touched and the ADC's have settled:
            else if (!display_isTouched() && addTimer == ADD_EXPIRED){
                // perform an increment/decrement depending on where the display
                // was touched.
                clockDisplay_performIncDec();
                // go back to state "1" and wait for the display to be touched
                CC_state = CC_waiting_for_touch;
            }
            break;

        // This state waits for the display to be touched for ~500ms before
        // quickly incrementing the display.
        case CC_auto_timer:
            // If the display is still touched and the timer has waited for
            // 500ms, go to the next state to start incremting the display quickly.
            if (display_isTouched() && autoTimer == AUTO_EXPIRED){
                // go to the next state
                CC_state = CC_rate_timer;
            }
            // if at any time the display is not touched, perform and increment or
            // decrement and go back to wait for the display to be touched again.
            else if (!display_isTouched()){
                // perform the increment or decrement
                clockDisplay_performIncDec();
                // go back to wait for the display to be touched again
                CC_state = CC_waiting_for_touch;
            }
            break;

        // We only want the display to increment 10 times per second, so this
        // state will wait ~100ms and then go the the state where the incrementing
        // or decremting happens
        case CC_rate_timer:
            // if the display is still touched by the time the counter expires
            if (display_isTouched() && rateTimer == RATE_EXPIRED){
                // go to the state where the incremting happens
                CC_state = CC_rate_timer_expired;
            }
            // otherwise, go back home
            else if (!display_isTouched()){
                // go back to waiting for the display to be touched again
                CC_state = CC_waiting_for_touch;
            }
            break;

        // Once we've waited 100ms, come here. This state performs the actual
        // incrementing or decrementing
        case CC_rate_timer_expired:
            // if the display is still touched:
            if (display_isTouched()){
                // perform the inc/dec and go wait for another 100ms to pass
                clockDisplay_performIncDec();
                CC_state = CC_rate_timer;
            }
            // if the display is no longer touched:
            else if (!display_isTouched()){
                // go back home and wait for the display to be touched again
                CC_state = CC_waiting_for_touch;
            }
            break;
    }
}
