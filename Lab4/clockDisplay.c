/*
 * clockDisplay.c
 *
 *  Created on: Sep 26, 2015
 *      Author: dwdegraw
 */

#include "clockDisplay.h"
// Include the display header so we can display things
#include "supportFiles/display.h"
// utils.h contains the utils_msDelay() function used to make the clock acurate
#include "supportFiles/utils.h"
// screenParams.h contains all the #defines that specify where on the screen to display things
#include "screenParams.h"
#include <stdio.h>

// this is used by sprintf() to format the hours as a decimal with no leading 0
#define FORMAT_HOURS "%2hd"
// this is used by sprintf() to format the minutes and seconds as a decimal with a leading 0
#define FORMAT_MINTUES_AND_SECONDS "%02hd"
// the maximum value for hours is 12, so this is used to detect if we need to roll
// hours back to 1
#define HOURS_ROLLOVER 13
// the minimum value for hours is 1, so this is used to detect if we need to rool
// hours up to 12
#define HOURS_ROLLUNDER 0
// the maximum value the hours can display
#define MAX_HOURS 12
// minutes and seconds should never actually display a 60, but instead roll over
// from 59 to 0 and increment the next unit 1. This is used to detect if we need
// to roll back to 0.
#define MINUTES_AND_SECONDS_ROLLOVER 60
// the minimum value seconds and hours can display is 0, so if we're already at 0
// we should roll back to the max value (59) instead of just decrementing
#define MINUTES_AND_SECONDS_ROLLUNDER 0
// the maximum value hours and seconds can ever display
#define MAX_MINUTES_AND_SECONDS 59

// Var to store the hours value in, as set by clockDisplay_advanceTimeOneSecond()
// initialize to 1 since hours are never 0
uint8_t newHours = 1;
// Var to store the minutes value in, as set by clockDisplay_advanceTimeOneSecond()
uint8_t newMinutes = 0;
// Var to store the seconds value in, as set by clockDisplay_advanceTimeOneSecond()
uint8_t newSeconds = 0;

// var to store the previous hours value in, used by clockDisplay_updateTimeDisplay()
// when determining whether to that number field needs to be updated on the display
// initialize to 1 since hours are never 0
uint8_t oldHours = 1;
// var to store the previous minutes value in, used by clockDisplay_updateTimeDisplay()
// when determining whether to that number field needs to be updated on the display
uint8_t oldMinutes = 0;
// var to store the previous seconds value in, used by clockDisplay_updateTimeDisplay()
// when determining whether to that number field needs to be updated on the display
uint8_t oldSeconds = 0;

// var to store converted and formatted hours value as returned by sprintf()
char charHours[2];
// var to store converted and formatted minutes value as returned by sprintf()
char charMin[2];
// var to store converted and formatted seconds value as returned by sprintf()
char charSec[2];


// This function initializes the display so it's ready for displaying the time
// After this has been called, the display is ready to go and there will be there
// six up/down triangles and the two colons seperating hours/minutes and minutes/seconds
void clockDisplay_init(){
    // initialize the dispaly so we can draw things on it
    display_init();
    // make the screen completely black
    display_fillScreen(DISPLAY_BLACK);
    // set the text size to the value specified in SCREENPARAMS_CLOCK_TEXT_SIZE
    display_setTextSize(SCREENPARAMS_CLOCK_TEXT_SIZE);
    // set the cursor to the scaled value for the first (hours) text spot
    display_setCursor(SCREENPARAMS_TEXT_1_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
    // set our text to green with a black background
    // black background is useful to completely overwrite the previous value,
    // otherwise it would be a green blob of text on top of text
    display_setTextColor(DISPLAY_GREEN, DISPLAY_BLACK);
    // display_println(1);
    // Set the cursor for the first colon. It's in between the hours and minutes values
    display_setCursor(SCREENPARAMS_TEXT_COLON_1_X, SCREENPARAMS_TEXT_CURSOR_Y);
    // Write the first colon to the screen
    display_println(SCREENPARAMS_CLOCK_COLON_SEPERATOR);
    // display_setCursor(SCREENPARAMS_TEXT_2_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
    // display_println(0)
    // Set the cursor for the second colon. It's in between the minutes and seconds values
    display_setCursor(SCREENPARAMS_TEXT_COLON_2_X, SCREENPARAMS_TEXT_CURSOR_Y);
    // Write the second colon to the screen
    display_println(SCREENPARAMS_CLOCK_COLON_SEPERATOR);
    // display_setCursor(SCREENPARAMS_TEXT_3_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
    // display_println(0);

    // Draw our 6 up/down triangles to the screen to the scaled positions.
    // Fill the triangle with green.
    display_fillTriangle(SCREENPARAMS_TRIANGLE2_X0, SCREENPARAMS_TRIANGLE2_Y0, SCREENPARAMS_TRIANGLE2_X1, SCREENPARAMS_TRIANGLE2_Y1, SCREENPARAMS_TRIANGLE2_X2, SCREENPARAMS_TRIANGLE2_Y2, DISPLAY_GREEN);
    display_fillTriangle(SCREENPARAMS_TRIANGLE1_X0, SCREENPARAMS_TRIANGLE1_Y0, SCREENPARAMS_TRIANGLE1_X1, SCREENPARAMS_TRIANGLE1_Y1, SCREENPARAMS_TRIANGLE1_X2, SCREENPARAMS_TRIANGLE1_Y2, DISPLAY_GREEN);
    display_fillTriangle(SCREENPARAMS_TRIANGLE3_X0, SCREENPARAMS_TRIANGLE3_Y0, SCREENPARAMS_TRIANGLE3_X1, SCREENPARAMS_TRIANGLE3_Y1, SCREENPARAMS_TRIANGLE3_X2, SCREENPARAMS_TRIANGLE3_Y2, DISPLAY_GREEN);
    display_fillTriangle(SCREENPARAMS_TRIANGLE4_X0, SCREENPARAMS_TRIANGLE4_Y0, SCREENPARAMS_TRIANGLE4_X1, SCREENPARAMS_TRIANGLE4_Y1, SCREENPARAMS_TRIANGLE4_X2, SCREENPARAMS_TRIANGLE4_Y2, DISPLAY_GREEN);
    display_fillTriangle(SCREENPARAMS_TRIANGLE5_X0, SCREENPARAMS_TRIANGLE5_Y0, SCREENPARAMS_TRIANGLE5_X1, SCREENPARAMS_TRIANGLE5_Y1, SCREENPARAMS_TRIANGLE5_X2, SCREENPARAMS_TRIANGLE5_Y2, DISPLAY_GREEN);
    display_fillTriangle(SCREENPARAMS_TRIANGLE6_X0, SCREENPARAMS_TRIANGLE6_Y0, SCREENPARAMS_TRIANGLE6_X1, SCREENPARAMS_TRIANGLE6_Y1, SCREENPARAMS_TRIANGLE6_X2, SCREENPARAMS_TRIANGLE6_Y2, DISPLAY_GREEN);

    clockDisplay_updateTimeDisplay(1);
}

// This function will update the display with the current values in charHours, charMin, and charSec
// If force is true, it will update all 3 fields, if it's false, it will only update the fields
// that have changed since the last time the display was updated
void clockDisplay_updateTimeDisplay(bool force){
    // If the hours have changed or force is true
    if (newHours != oldHours || force){
        // update the old hours time to signify it's been written to the screen
        oldHours = newHours;
        // convert the current hours value to a char array
        sprintf(charHours, FORMAT_HOURS, newHours);
        // set the cursor to the appropriate postion
        display_setCursor(SCREENPARAMS_TEXT_1_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
        // write the hours value to the display
        display_println(charHours);
    }

    // If the minutes have changed or force is true
    if (newMinutes != oldMinutes || force){
        // update the old minutes time to signify it's been written to the screen
        oldMinutes = newMinutes;
        // convert the current minutes value to a char array
        sprintf(charMin, FORMAT_MINTUES_AND_SECONDS, newMinutes);
        // set the cursor to the appropriate postion
        display_setCursor(SCREENPARAMS_TEXT_2_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
        // write the minutes value to the display
        display_println(charMin);
    }

    if (newSeconds != oldSeconds || force){
        oldSeconds = newSeconds;
        // convert the current seconds value to a char array
        sprintf(charSec, FORMAT_MINTUES_AND_SECONDS, newSeconds);
        // set the cursor to the appropriate postion
        display_setCursor(SCREENPARAMS_TEXT_3_CURSOR_X, SCREENPARAMS_TEXT_CURSOR_Y);
        // write the seconds value to the display
        display_println(charSec);
    }
}

// this will increment the hours and perform the needed rollover from 12 to 1
void incHours(){
    // increment hours by one
    newHours += 1;
    // if hours are 13, reset it to 1
    if (newHours == HOURS_ROLLOVER){
        newHours = 1;
    }
}

// this will decrement the hours and perform the needed rollover from 1 to 12
void decHours(){
    // decrement hours
    newHours -= 1;
    // if hours is at 0, reset them to 12
    if (newHours == HOURS_ROLLUNDER){
        newHours = MAX_HOURS;
    }
}

// this will increment the minutes and perform the needed rollover from 59 to 0
void incMin(){
    // increment mintues
    newMinutes += 1;
    // if minutes is at 60, reset them back to 0
    if (newMinutes == MINUTES_AND_SECONDS_ROLLOVER){
        newMinutes = 0;
    }
}

// this will decrement the minutes and perform the needed rollover from 0 to 59
void decMin(){
    // if minutes are already at the lowest (0), roll them back to 59
    if (newMinutes == MINUTES_AND_SECONDS_ROLLUNDER){
        newMinutes = MAX_MINUTES_AND_SECONDS;
    }
    // otherwise just decement them by one
    else{
        newMinutes -= 1;
    }
}

// this will increment the seconds and perform the needed rollover from 59 to 0
void incSec(){
    // increment seconds
    newSeconds += 1;
    // if seconds is at 60, reset them back to 0
    if (newSeconds == MINUTES_AND_SECONDS_ROLLOVER){
        newSeconds = 0;
    }
}

// this will decrement the seconds and perform the needed rollover from 0 to 59
void decSec(){
    // if seconds are already at the lowest (0), roll them back to 59
    if (newSeconds == MINUTES_AND_SECONDS_ROLLUNDER){
        newSeconds = MAX_MINUTES_AND_SECONDS;
    }
    // otherwise just decement them by one
    else{
        newSeconds -= 1;
    }
}

// this fuction will examine where the screen was touched, and perform the
// necessary increment or decrement operation depending on which field was touched
void clockDisplay_performIncDec(){
    // set up 3 variables to hold the X, Y, and Z (pressure) returned by display_getTouchedPoint
    int16_t x = 0;
    int16_t y = 0;
    // note that we don't actually care about Z (pressure), but we have to store it anyways
    uint8_t z = 0;

    // get the last touched point and store it in X, Y, and Z
    display_getTouchedPoint(&x, &y, &z);

    if (y < 120){ // top half was pushed, so we'll need to increment
        if (x < 106){ // hours up was pushed, so increment hours
            incHours();
        }
        else if (x < 213){ // minutes was pushed, so increment minutes
            incMin();
        }
        else{ // safe to assume seconds was pushed, so increment seconds
            incSec();
        }
    }
    else if (y > 120){ // bottom half was pushed, so we'll need to decrement
        if (x < 106){ // hours down was pushed, so decrement hours
            decHours();
        }
        else if (x < 213){ // hours down was pushed, so decrement hours
            decMin();
        }
        else{ // safe to assume seconds was pushed, so decrement seconds
            decSec();
        }
    }

    // update the display with the new values
    clockDisplay_updateTimeDisplay(0);
}
// This function will add one second the the current time values.
// It handles rolling seconds over to minutes, and minutes over to hours,
// and rolling hours from 12 back to 1.
void clockDisplay_advanceTimeOneSecond(){
    // Increase seconds by 1
    newSeconds += 1;

    // if seconds is 60, increase minutes by one and make seconds 0
    if (newSeconds == 60){
        newSeconds = 0;
        newMinutes += 1;
    }
    // if minutes is 60, increase hours by one and make minutes 0
    if (newMinutes == 60){
        newMinutes = 0;
        newHours += 1;
    }
    // if minutes is 13, roll over back to 1
    if (newHours == 13){
        newHours = 1;
    }
}

// This function runs a test to ensure hours, minutes, and seconds can be both
// incremented and decremented.
// After that, it will run the clock at 10x normal speed for 10 seconds and then stop.
void clockDisplay_runTest(){
    // Initialize the clock
    clockDisplay_init();
    // Update the clock so the displayed time is 1:00:00.
    // Pass true in to make sure all fields are updated.
    clockDisplay_updateTimeDisplay(1);

    // 5 times do:
    for (int i = 0; i < 5; i++){
        // increment hours
        newHours += 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // 5 times do:
    for (int i = 0; i < 5; i++){
        // decrement hours
        newHours -= 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // 5 times do:
    for (int i = 0; i < 5; i++){
        // increment hours
        newMinutes += 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // 5 times do:
    for (int i = 0; i < 5; i++){
        newMinutes -= 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // 5 times do:
    for (int i = 0; i < 5; i++){
        // increment hours
        newSeconds += 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // 5 times do:
    for (int i = 0; i < 5; i++){
        newSeconds -= 1;
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait half a second
        utils_msDelay(500);
    }

    // allow the clock to tick at 10x normal speed for 10 seconds
    for (int i = 0; i < 100; i++){
        // tick one second
        clockDisplay_advanceTimeOneSecond();
        // update the display
        clockDisplay_updateTimeDisplay(0);
        // wait 1/10th of a second
        utils_msDelay(100);
    }
}
