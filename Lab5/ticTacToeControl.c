///*
// * ticTacToeControl.c
// *
// *  Created on: Oct 26, 2015
// *      Author: dwdegraw
// */
//#include "ticTacToeControl.h"
//#include <stdio.h>
//#include "ticTacToeDisplay.h"
//#include "supportFiles/display.h"
//#include "minimax.h"
//#include "../Lab2/buttons.h"
//
//#define START_MESSAGE_EXPIRED 7
//#define WAIT_TIMER_EXPIRED 10
//#define ADC_IS_SETTLED 1
//#define BUTTON_0_MASK 0x01
//
//#define X_THIRD (DISPLAY_WIDTH/3)
//// one sixth of the display width
//// used for drawing the O's in the middle of each square
//#define X_SIXTH (DISPLAY_WIDTH/6)
//// one third of the display height
//// used for calculating our two horizontal lines
//#define Y_THIRD (DISPLAY_HEIGHT/3)
//// one sixth of the display height
//// used for drawing the O's in the middle of each square
//#define Y_SIXTH (DISPLAY_HEIGHT/6)
//// one twelfth of the display width
//// used for drawing the X's
//#define X_TWELFTH (DISPLAY_WIDTH/12)
//// one twelfth of the display height
//// used for drawing the X's
//#define Y_TWELFTH (DISPLAY_HEIGHT/12)
//// define how big each O should be
//// this was found by trial and error until a good value was found
//#define O_RADIUS 30
//// the multiplier for column 0
//
//uint8_t boardInitialized = 0;
//uint8_t displayInitialized = 0;
//uint8_t startMessageCounter = 0;
//uint8_t waitTimerCounter = 0;
//uint8_t adcSettleCounter = 0;
//uint8_t computerIsX = 0; // false if computer is x, true if player is x
//uint8_t playerChoiceValid = 0;
//uint8_t playerMovedLast = 0;
//uint8_t firstMove = 1;
//uint8_t mrow = 255;
//uint8_t mcolumn = 255;
//minimax_move_t nextMove;
//minimax_board_t bboard;
//minimax_board_t bboarddupe;
//
//
//// States for the controller state machine.
//enum ticTacToe_st_t {
//    init_st,                 // Start here, stay in this state for just one tick.
//    start_message_st,        // Display the start message and instructions for 2 seconds
//    draw_board_st,           // Draw the board. Wait for 4 seconds. If screen is touched, player goes first. Otherwise computer goes first.
//    wait_for_touch_st,
//    display_untouched_st,
//    player_turn_st,          // Let the player make a move
//    adc_settle_st,           // Let the ADC settle
//    computer_turn_st,        // Let the computer make a move
//    check_score_st,
//    new_game_st,
//    end_game_st              // Once the game is over, wait here till button 0 is pressed
//} currentState = init_st;
//
//void eraseStartMessage() {
//    if (!displayInitialized){
//        display_setCursor(0,0);
//        display_setTextSize(2);
//        display_setTextColor(DISPLAY_BLACK);
//        display_println("Touch to play as X\n--OR--\nWait to play as O");
//        displayInitialized = 1;
//    }
//}
//
//void eraseBoard() {
//    for (int r = 0; r < 3; r++){
//        for (int c = 0; c < 3; c++){
//            display_drawLine((X_THIRD*r)+X_TWELFTH, (Y_THIRD*c) + Y_TWELFTH, (X_THIRD*(r+1))-X_TWELFTH, (Y_THIRD*(c+1)) - Y_TWELFTH, DISPLAY_BLACK);
//            // this draws a line from the bottom left of the square to the top right
//            display_drawLine((X_THIRD*(r))+X_TWELFTH, (Y_THIRD*(c+1)) - Y_TWELFTH, (X_THIRD*(r+1))-X_TWELFTH, (Y_THIRD*c) + Y_TWELFTH, DISPLAY_BLACK);
//            int16_t x =(X_THIRD*r) + X_SIXTH;
//            // set the y value of the circle to be drawn to be in the middle of the column specified
//            int16_t y = (Y_THIRD*c) + Y_SIXTH;
//            // actually draw the circle with our X, Y, RADIUS, with a color of green
//            display_drawCircle(x, y, O_RADIUS, DISPLAY_BLACK);
//        }
//    }
//}
//
//void initializeBoard() {
////    if (!boardInitialized) {
//        for (int i = 0; i < 3; i++){
//            for (int i2 = 0; i2 < 3; i2++){
//                bboard.squares[i][i2] = MINIMAX_EMPTY_SQUARE;
////                printf("Square is: %d\n", bboard.squares[i][i2]);
//            }
//        }
////    }
////    boardInitialized = 1;
//}
//
//void findEmptySquare(){
//    for (int i = 0; i < 3; i++){
//        for (int i2 = 0; i2 < 3; i2++){
//            if (bboard.squares[i][i2] == MINIMAX_EMPTY_SQUARE){
//                mrow = i;
//                mcolumn = i2;
//            }
//            printf("Square is: %d\n", bboard.squares[i][i2]);
//        }
//    }
//}
//
//void playerChoiceIsValid(){
//    if (bboard.squares[mrow][mcolumn] != MINIMAX_EMPTY_SQUARE){
//        playerChoiceValid = 0;
//    }
//    else {
//        playerChoiceValid = 1;
//    }
//}
//
//void ticTacToeControl_tick() {
//  // Perform state action first.
//  switch(currentState) {
//    case init_st:
//        initializeBoard();
//        break;
//    case start_message_st:
//        startMessageCounter++;
//        break;
//    case draw_board_st:
//        eraseStartMessage();
//        ticTacToeDisplay_drawBoardLines();
//        break;
//    case display_untouched_st:
//        break;
//    case player_turn_st:
//        ticTacToeDisplay_touchScreenComputeBoardRowColumn(&mrow, &mcolumn);
//        playerChoiceIsValid();
//        if (!playerChoiceValid){
//            bboard.squares[mrow][mcolumn] = MINIMAX_PLAYER_SQUARE;
//            break;
//        }
//        if (computerIsX){
//            ticTacToeDisplay_drawO(mrow, mcolumn);
//        }
//        else {
//            ticTacToeDisplay_drawX(mrow, mcolumn);
//        }
//        playerMovedLast = 1;
//        bboard.squares[mrow][mcolumn] = MINIMAX_PLAYER_SQUARE;
//        break;
//    case adc_settle_st:
//        adcSettleCounter++;
//        break;
//    case computer_turn_st:
//        printf("computer's turn\n");
//        minimax_computeNextMove(&bboard, computerIsX, &mrow, &mcolumn);
//        if (mrow > 2 || mrow < 0 || mcolumn > 2 || mcolumn < 2){
//            findEmptySquare();
//        }
//        if (computerIsX){
//            ticTacToeDisplay_drawX(mcolumn, mrow);
//        }
//        else {
//            ticTacToeDisplay_drawO(mcolumn, mrow);
//        }
//        playerMovedLast = 0;
//        bboard.squares[mrow][mcolumn] = MINIMAX_OPPONENT_SQUARE;
//        break;
//    case end_game_st:
//        break;
//    case wait_for_touch_st:
//        waitTimerCounter++;
//        break;
//    case check_score_st:
//        break;
//    case new_game_st:
//        initializeBoard();
//        eraseBoard();
//        break;
//    default:
//        printf("clockControl_tick state action: hit default\n\r");
//        break;
//  }
//
//  // Perform state update next.
//  switch(currentState) {
//    case init_st:
//        currentState = start_message_st;
//        break;
//    case start_message_st:
//        if (startMessageCounter == START_MESSAGE_EXPIRED){
//            startMessageCounter = 0;
//            currentState = draw_board_st;
//        }
//        break;
//    case draw_board_st:
//        currentState = wait_for_touch_st;
//        break;
//    case display_untouched_st:
//        if (!display_isTouched()){
//            currentState = adc_settle_st;
//        }
//        break;
//    case player_turn_st:
//        mrow = -1;
//        mcolumn = -1;
//        if (!playerChoiceValid) {
//            break;
//        }
//        else {
//            currentState = check_score_st;
//            playerChoiceValid = 0;
//            break;
//        }
//    case adc_settle_st:
//        if (adcSettleCounter == ADC_IS_SETTLED){
//            adcSettleCounter = 0;
//            currentState = player_turn_st;
//        }
//        break;
//    case computer_turn_st:
//        mrow = -1;
//        mcolumn = -1;
//        currentState = check_score_st;
//        break;
//    case end_game_st:
//        if (buttons_read() & BUTTON_0_MASK){
//            boardInitialized = 0;
//            currentState = new_game_st;
//        }
//        break;
//    case wait_for_touch_st:
//        if (waitTimerCounter == WAIT_TIMER_EXPIRED && firstMove){
//            waitTimerCounter = 0;
//            computerIsX = 1;
//            firstMove = 0;
//            currentState = computer_turn_st;
//        }
//        else if (display_isTouched()){
//            waitTimerCounter = 0;
//            if (firstMove) {
//                computerIsX = 0;
//                firstMove = 0;
//            }
//            display_clearOldTouchData();
//            currentState = display_untouched_st;
//        }
//        break;
//    case check_score_st:
//        bboarddupe = bboard;
//        if (minimax_computeBoardScore(&bboarddupe, computerIsX) != -1 || minimax_computeBoardScore(&bboarddupe, !computerIsX) != -1){
//            currentState = end_game_st;
//        }
//        else if (playerMovedLast){
//            currentState = computer_turn_st;
//        }
//        else {
//            currentState = wait_for_touch_st;
//        }
//        break;
//    case new_game_st:
//        firstMove = 1;
//        initializeBoard();
//        currentState = draw_board_st;
//        break;
//    default:
//        printf("clockControl_tick state update: hit default\n\r");
//        break;
//  }
//}
//
//
