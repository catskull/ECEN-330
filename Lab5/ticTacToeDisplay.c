/*
 * ticTacToeDisplay.c
 *
 *  Created on: Oct 14, 2015
 *      Author: dwdegraw
 */
#include "ticTacToeDisplay.h"
// include the display file so we can use the screen
#include "supportFiles/display.h"
// include utils so we can delay while waiting for the ADC's to settle
#include "supportFiles/utils.h"
//
#include "../Lab2/buttons.h"
#include "../Lab2/switches.h"
#include <stdio.h>

// one third of the display width
//used for calculating our two vertical lines
#define X_THIRD (DISPLAY_WIDTH/3)
// one sixth of the display width
// used for drawing the O's in the middle of each square
#define X_SIXTH (DISPLAY_WIDTH/6)
// one third of the display height
// used for calculating our two horizontal lines
#define Y_THIRD (DISPLAY_HEIGHT/3)
// one sixth of the display height
// used for drawing the O's in the middle of each square
#define Y_SIXTH (DISPLAY_HEIGHT/6)
// one twelfth of the display width
// used for drawing the X's
#define X_TWELFTH (DISPLAY_WIDTH/12)
// one twelfth of the display height
// used for drawing the X's
#define Y_TWELFTH (DISPLAY_HEIGHT/12)
// define how big each O should be
// this was found by trial and error until a good value was found
#define O_RADIUS 30
// the multiplier for column 0
#define COLUMN_0 0
// the multiplier for column 1
#define COLUMN_1 1
// the multiplier for column 2
#define COLUMN_2 2
// the multiplier for row 0
#define ROW_0 0
// the multiplier for row 1
#define ROW_1 1
// the multiplier for row 2
#define ROW_2 2
// the delay in milleseconds to wait for the touch screen ADC's to "settle"
// this was given in the lab instructions
#define TOUCH_SCREEN_DELAY 50
// the size of the text
// for milestone one, the only text drawn is the end test message
#define TEXT_SIZE 2
// a mask to detect the state of switch 1
#define SWITCH_0_MASK 0x01
// a mask to detect the state of button 1
#define BUTTON_0_MASK 0x01
// a mask to detect the state of button 2
#define BUTTON_1_MASK 0x02
// the text to display when the test has been exited
#define END_TEST_TEXT "PLAYTIME IS OVER"

// this function will set up the display for a new game of tic tac toe
void ticTacToeDisplay_init(){
    display_init();
    display_fillScreen(DISPLAY_BLACK);
    display_setTextSize(2);
    display_setTextColor(DISPLAY_GREEN);
    display_println("Touch to play as X\n--OR--\nWait to play as O");
}

// this function will draw an "X" on the screen
// row: valid values are 0 - 2. Row 0 is the top row on the screen
// column: valid values are 0 - 2. Column 0 is the farthest left row one the screen.
// Example: ticTacToeDisplay_drawX(0, 0) will draw an "X" in the top left corner.
void ticTacToeDisplay_drawX(uint8_t row, uint8_t column){
    // each X consists of two single lines that intersect in the middle of each square.
    // this draws a line from the top left of the square to the bottom right
    display_drawLine((X_THIRD*row)+X_TWELFTH, (Y_THIRD*column) + Y_TWELFTH, (X_THIRD*(row+1))-X_TWELFTH, (Y_THIRD*(column+1)) - Y_TWELFTH, DISPLAY_GREEN);
    // this draws a line from the bottom left of the square to the top right
    display_drawLine((X_THIRD*(row))+X_TWELFTH, (Y_THIRD*(column+1)) - Y_TWELFTH, (X_THIRD*(row+1))-X_TWELFTH, (Y_THIRD*column) + Y_TWELFTH, DISPLAY_GREEN);
}

// this function will draw an "O" on the screen
// row: valid values are 0 - 2. Row 0 is the top row on the screen
// column: valid values are 0 - 2. Column 0 is the farthest left row one the screen.
// Example: ticTacToeDisplay_drawO(0, 0) will draw an "O" in the top left corner.
void ticTacToeDisplay_drawO(uint8_t row, uint8_t column){
    // drawing an "O" is easier programatically than drawing an X
    // for simplicity's sake, we'll store our X and Y values
    // set the x value of the circle to be drawn to be in the middle of the row specified
    int16_t x =(X_THIRD*row) + X_SIXTH;
    // set the y value of the circle to be drawn to be in the middle of the column specified
    int16_t y = (Y_THIRD*column) + Y_SIXTH;
    // actually draw the circle with our X, Y, RADIUS, with a color of green
    display_drawCircle(x, y, O_RADIUS, DISPLAY_GREEN);
}

// this function will convert the touched X-Y coordinates to a column and row
// to be used by ticTacToeDisplay_drawX or ticTacToeDisplay_drawO
// row: a location in memory to return the decoded y coordinate
// column: a location in memory to return the decoded x coordinate
void ticTacToeDisplay_touchScreenComputeBoardRowColumn(uint8_t* row, uint8_t* column){
    // set up the variables needed by display_getTouchedPoint
    int16_t x;
    int16_t y;
    // note that we don't care about Z (pressure), but we have to send it to display_getTouchedPoint
    uint8_t z;
    // get the last touched point in terms of X, Y, and Z
    display_getTouchedPoint(&x, &y, &z);
    // if the Y is in the top third of the screen (or the top row)
    if (y <= Y_THIRD){
        // set the column to be the let, or 0
        *column = COLUMN_0;
    }
    // if the Y is in the second third of the screen (or the middle row)
    else if (y <= Y_THIRD*ROW_2){
        // set the column to be the middle, or 1
        *column = COLUMN_1;
    }
    // if the Y is not in the first two thirds of the screen,
    // it's safe to assume it's in the right third (or the right column)
    else {
        // set the column to be the right, or 2
        *column = COLUMN_2;
    }

    // if the X is in the left most third of the screen (or the left most row)
    if (x <= X_THIRD){
        // set the row to be the top, or 0
        *row = ROW_0;
    }
    // if the X is in the middle third of the screen (or the middle row)
    else if (x <= X_THIRD*COLUMN_2){
        // set the row to be the middle, or 1
        *row = ROW_1;
    }
    // if the X is not in the top two thirds of the screen,
    // it's safe to assume it's in the right third (or the bottom row)
    else {
        // set the row to be the bottom, or 2
        *row = ROW_2;
    }
}

// this function will run a test specified in the lab requirements, that is:
// initialize the tic tac toe game with the four lines,
// detect where the display was touched and draw either an "X" or an "O" in that square
// if switch 1 is on, draw an "O", if it's off draw an "X"
// if button 0 is pressed, clear the screen and redraw the lines
// if button 1 is pressed, clear the screen, end the test, and draw text to indicate the test has ended
void ticTacToeDisplay_runTest(){
    // initialize the display
    ticTacToeDisplay_init();
    // initialize the switches
    switches_init();
    // initialize the buttons
    buttons_init();

    // the cursor to a position near the middle of the display
    display_setCursor(X_SIXTH, Y_THIRD);
    // make the text color green
    display_setTextColor(DISPLAY_GREEN);
    // set the text size to the value of TEXT_SIZE (which is 2)
    display_setTextSize(TEXT_SIZE);

    // set up variables to store the row and column returned by ticTacToeDisplay_touchScreenComputeBoardRowColumn()
    uint8_t row;
    uint8_t column;

    // loop forever, or until button 1 is pressed
    while(1){
        // if the display is touched
        if(display_isTouched()){
            // clear old touch data
            display_clearOldTouchData();
            // wait for the ADC's to settle
            utils_msDelay(TOUCH_SCREEN_DELAY);
            // decode the touch to a row and column
            ticTacToeDisplay_touchScreenComputeBoardRowColumn(&row, &column);
            // if switch 0 is on, draw an "O"
            if (switches_read() & SWITCH_0_MASK){
                ticTacToeDisplay_drawO(row, column);
            }
            // if switch 0 is off, draw an "X"
            else {
                ticTacToeDisplay_drawX(row, column);
            }
        }
        // when button 0 is pressed:
        else if (buttons_read() & BUTTON_0_MASK){
            // clear the screen
            display_fillScreen(DISPLAY_BLACK);
            // redraw the board lines
            ticTacToeDisplay_drawBoardLines();
        }
        // when button 1 is pressed:
        else if (buttons_read() & BUTTON_1_MASK){
            // clear the screen
            display_fillScreen(DISPLAY_BLACK);
            // display the test end text ("PLAYTIME IS OVER")
            display_println(END_TEST_TEXT);
            // exit the endless while loop
            break;
        }
    }
}

// this function will draw the 4 green lines needed for a tic tac toe board
// We're using display_drawFastVLine and display_drawFastHLine simply because they're faster
// they're faster because those functions can only draw straight lines, which is perfect for the board lines,
// but these functions wouldn't work for drawing the "X"'s
void ticTacToeDisplay_drawBoardLines(){
    // draw the first vertical line at one third of the display width and the entire height of the screen
    display_drawFastVLine(X_THIRD, 0, DISPLAY_HEIGHT, DISPLAY_GREEN);
    // draw the first vertical line at two thirds of the display width and the entire height of the screen
    display_drawFastVLine(X_THIRD*COLUMN_2, 0, DISPLAY_HEIGHT, DISPLAY_GREEN);
    // draw the first horizontal line at one third of the display height and the entire width of the screen
    display_drawFastHLine(0, Y_THIRD, DISPLAY_WIDTH, DISPLAY_GREEN);
    // draw the first horizontal line at two thirds of the display height and the entire width of the screen
    display_drawFastHLine(0, Y_THIRD*COLUMN_2, DISPLAY_WIDTH, DISPLAY_GREEN);
}
