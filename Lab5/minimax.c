
/*
 * minimax.c
 *
 *  Created on: Oct 15, 2015
 *      Author: dwdegraw
 */

// include the minimax header file supplied by hutch
#include "minimax.h"

// a tic tac toe board is 3x3 for a total of 9 squares
// this is used to keep our scores and moves lists
#define MINIMAX_BOARD_SIZE 9

// this a holder for the next move to make
minimax_move_t choice;



// this is the "brain" of the program.
// it will look at every possible move and chose the one that is most likely
// to win, or the fastest to win
// board: the current tic tac toe board being played on
// player: who's turn is it? true is player's turn, false is computer's turn
int minimax(minimax_board_t* board, bool player){

    choice.column = -1;
    choice.row = -1;

    // first thing to do is determine the score of the board in it's
    // current state
    minimax_score_t score = minimax_computeBoardScore(board, !player);

    // then we want to check if the game is over
    // because if it is, we don't need to do anything
    // besides returning the score
    if (minimax_isGameOver(score)){
        return score;
    }

    // this index is used to address our scores-moves arrays
    // we look in the scores array to find the highest one,
    // then go look for the corresponding moves array
    int index = 0;

    // an array to store all the scores
    minimax_score_t scores[MINIMAX_BOARD_SIZE] = {};
    // an array to store the moves that got us to the scores
    minimax_move_t moves[MINIMAX_BOARD_SIZE] = {};

    // for every row
    for (int row = 0; row < MINIMAX_BOARD_ROWS; row++){
        // for every column
        for (int column = 0; column < MINIMAX_BOARD_COLUMNS; column++){
            // if the square is empty, it's a testable scenario
            if (board->squares[row][column] == MINIMAX_EMPTY_SQUARE){
                // if the player is making a move
                if (player){
                    // play on the square
                    board->squares[row][column] = MINIMAX_PLAYER_SQUARE;
                }
                // if the computer is making a move
                else {
                    // play on the square
                    board->squares[row][column] = MINIMAX_OPPONENT_SQUARE;
                }
                // now recursively call this function to
                // play the board to completion
                // alternate the player, taking turns normally
                score = minimax(board, !player);
                // add the score to the scores array
                scores[index] = score;
                // add the move row to the moves array
                moves[index].row = row;
                // add the move column to the moves array
                moves[index].column = column;
                // set the square back to empty to return the board
                // to the state in which it was passed originally
                board->squares[row][column] = MINIMAX_EMPTY_SQUARE;
                // iterate our shared index
                index++;
            }
        }
    }

    // this is the highest score we got
    // it will be changed every time a higher score is found
    minimax_score_t tempScore = 0;

//    choice.row = moves[-1].row;
//    choice.column = moves[-1].column;

    // if the player is player, we want the max score
    if (player){
        // go through all the scores in the scores array
        for (int i = 0; i < MINIMAX_BOARD_SIZE; i++){
            // if the score at the current position is greater
            // than tempScore's current value, set it to the higher score
            if (scores[i] > tempScore){
                // set tempScore to the higher score
                tempScore = scores[i];
                // set the master score to the current score
                score = scores[i];
                // set the next move's row and column
                choice.row = moves[i].row;
                choice.column = moves[i].column;
            }
        }
    }
    // if the computer is player, we want the min score
    else {
        // go through the entire score array
        for (int i = 0; i < MINIMAX_BOARD_SIZE; i++){
            // if the current score is lower than tempScore
            if (scores[i] < tempScore){
                // set tempScore
                tempScore = scores[i];
                // set the master score
                score = scores[i];
                // set the next move's row and column
                choice.row = moves[i].row;
                choice.column = moves[i].column;
            }
        }
    }

    if (choice.row == -1 && choice.column == -1){
        choice.row = moves[0].row;
        choice.column = moves[0].column;
    }

    // return the master score
    return score;
}

// this function will determine the best place to play next
// board: the current tic tac toe board being played on
// player: true if the player is playing, false if the computer is playing
// row: the row which the next move should be played
// column: the column which the next move should be played
void minimax_computeNextMove(minimax_board_t* board, bool player, uint8_t* row, uint8_t* column){
    // call our recursive minimax function on the current board
    minimax(board, player);
    // set row to the row of the best move to make
    *row = choice.row;
    // set column to the column of the best move to make
    *column = choice.column;
}

// this simply checks to see if the score such that the game
// is now over.
// The game is over if the score is 10, -10, or 0 (draw)
// returns true if the game is over, false otherwise
bool minimax_isGameOver(minimax_score_t score){
    // if the gmae is over
    if (score == MINIMAX_PLAYER_WINNING_SCORE ||
            score == MINIMAX_OPPONENT_WINNING_SCORE ||
            score == MINIMAX_DRAW_SCORE){
        // return true
        return 1;
    }
    // return false
    return 0;
}

// this function determines if the current board is a win, lose, draw,
// or undetermined.
// returns 10 if player won, -10 if computer won, 0 if a draw, or -1 if the game is still going
// board: the current tic tac toe board being played
// player: true if the player is a player, false if player is the computer
int16_t minimax_computeBoardScore(minimax_board_t* board, bool player){
    // a variable to store which type of squares we're looking for
    int currentPlayerSquare;
    // a variable to store what the current player's winning score is (10 or -10)
    int winningScore;

    // if player is true
    if (!player){
        // we want to look for the player's squares
        currentPlayerSquare = MINIMAX_PLAYER_SQUARE;
        // we want to return the player's winning score (10)
        winningScore = MINIMAX_PLAYER_WINNING_SCORE;
    }
    // if player is false
    else {
        // we want to look for the opponent's squares
        currentPlayerSquare = MINIMAX_OPPONENT_SQUARE;
        // we want to return the opponent's winning score (-10)
        winningScore = MINIMAX_OPPONENT_WINNING_SCORE;
    }

    // this will check all the columns to see if there's 3 in a row
    for (int column = 0; column < MINIMAX_BOARD_COLUMNS; column++){
        for (int row = 0; row < MINIMAX_BOARD_ROWS; row++){
            // if the sqare isn't the one we're looking for, quit searching that column
            if (board->squares[column][row] != currentPlayerSquare){
                break;
            }
            // if the square is on the bottom row, we have won!
            if (row == 2 && board->squares[column][row] == currentPlayerSquare){
                return winningScore;
            }
        }
    }

    // this will check all the rows to see if there's 3 in a row
    for (int row = 0; row < MINIMAX_BOARD_ROWS; row++){
        for (int column = 0; column < MINIMAX_BOARD_COLUMNS; column++){
            // if the square isn't the one we're looking for, quit searching that row
            if (board->squares[column][row] != currentPlayerSquare){
                break;
            }
            // if the square is on the right most column, we have won!
            if (column == (MINIMAX_BOARD_COLUMNS - 1) && board->squares[column][row] == currentPlayerSquare){
                return winningScore;
            }
        }
    }

    // this checks diagonally from top left to bottom right for 3 in a row
    if (board->squares[0][0] == currentPlayerSquare &&
        board->squares[1][1] == currentPlayerSquare &&
        board->squares[(MINIMAX_BOARD_ROWS - 1)][(MINIMAX_BOARD_COLUMNS - 1)] == currentPlayerSquare){
        return winningScore;
    }

    // this checks diagonally from bottom left to top right for 3 in a row
    if (board->squares[0][(MINIMAX_BOARD_COLUMNS - 1)] == currentPlayerSquare &&
        board->squares[1][1] == currentPlayerSquare &&
        board->squares[(MINIMAX_BOARD_ROWS - 1)][0] == currentPlayerSquare){
        return winningScore;
    }

    // if we haven't won and there are still empty squares, the game isn't over
    for (int column = 0; column < (MINIMAX_BOARD_COLUMNS - 1); column++){
        for (int row = 0; row < (MINIMAX_BOARD_ROWS - 1); row++){
            if (board->squares[row][column] == MINIMAX_EMPTY_SQUARE){
                return MINIMAX_NOT_ENDGAME;
            }
        }
    }

    // we didn't win or lose, and the board is totally full so it's a draw
    return MINIMAX_DRAW_SCORE;
}

// this function will set the board to all empty squares
// board: the board being played on
void minimax_initBoard(minimax_board_t* board){
    // for all rows
    for (int row = 0; row < MINIMAX_BOARD_ROWS; row++){
        // for all columns
        for (int column = 0; column < MINIMAX_BOARD_COLUMNS; column++){
            // set the sqare to be empty
            board->squares[row][column] = MINIMAX_EMPTY_SQUARE;
        }
    }
}
