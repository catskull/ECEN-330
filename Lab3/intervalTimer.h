/*
 * intervalTimer.h
 *
 *  Created on: Sep 17, 2015
 *      Author: dwdegraw
 */

#ifndef INTERVALTIMER_H_
#define INTERVALTIMER_H_
// all functions return this value if they worked
#define INTERVALTIMER_SUCCESS 0x1
// some functions return this if you passed an invalid parameter
#define INTERVALTIMER_FAILURE 0x0
#include <stdint.h>

// start the timer number you specify
// valid inputs are 0, 1, or 2
uint32_t intervalTimer_start(uint32_t);

// stop the timer number you specify
// valid inputs are 0, 1, or 2
uint32_t intervalTimer_stop(uint32_t);

// reset the timer number you specify
// valid inputs are 0, 1, or 2
uint32_t intervalTimer_reset(uint32_t);

// initialize the timer number you specify
// valid inputs are 0, 1, or 2
uint32_t intervalTimer_init(uint32_t);

// initialize all 3 timers
uint32_t intervalTimer_initAll();

// reset all 3 timers
uint32_t intervalTimer_resetAll();

// run intervalTimer_runTest on all 3 timers
uint32_t intervalTimer_testAll();

// run a test function to ensure the timer specified works correctly
// valid inputs are 0, 1, or 2
uint32_t intervalTimer_runTest(uint32_t);

// convert the value in a timer to seconds
// the second param will be set to the value in seconds
uint32_t intervalTimer_getTotalDurationInSeconds(uint32_t, double);


#endif /* INTERVALTIMER_H_ */
