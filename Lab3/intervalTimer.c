/*
 * intervalTimer.c
 *
 *  Created on: Sep 17, 2015
 *      Author: dwdegraw
 */

#include "intervalTimer.h"
#include "xparameters.h"
#include "xil_io.h"
#include <stdio.h>

// timer 1 offset
#define INTERVALTIMER_TCSR0 0x0
// timer 1 load register
#define INTERVALTIMER_TLR0 0x04
// timer 1 value register
#define INTERVALTIMER_TCR0 0x08
// timer 2 offset
#define INTERVALTIMER_TCSR1 0x10
// timer 2 value register
#define INTERVALTIMER_TCR1 0x18
// value to clear a register
#define INTERVALTIMER_CLEAR 0x0
// value to cause a timer to load the load register
#define INTERVALTIMER_LOAD0 0x20
// bit to set on timer 1 to cascade the two timers
#define INTERVALTIMER_CASC 0x800
#define INTERVALTIMER_ENT0 0x80
// frequency of the clock to calculate cycles to seconds
#define INTERVALTIMER_CLOCK_FREQ XPAR_AXI_TIMER_0_CLOCK_FREQ_HZ
// Integer value for timer 2
#define INTERVALTIMER_TIMER2 2
// Amount to shift lower INTERVALTIMER_MAX_TIMER_VAL2 bits to the upper INTERVALTIMER_MAX_TIMER_VAL2 bits of a 64 bit uint64
#define INTERVALTIMER_SHIFT32
// The limit when cycling through all timers
//#define INTERVALTIMER_MAX_TIMER_VAL INTERVALTIMER_MAX_TIMER_VAL

// Decode a number for a counter and return an address for it
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint32_t getTimerAddress(uint32_t timerNumber){
    // If 0 was passed, get timer 0 address
    if (timerNumber == 0){
        // return the address for timer 0
        return XPAR_AXI_TIMER_0_BASEADDR;
    }
    // If 1 was passed, get timer 1 address
    else if (timerNumber == 1){
        // return the address for timer 1
        return XPAR_AXI_TIMER_1_BASEADDR;
    }
    // If 2 was passed, get timer 2 address
    else if (timerNumber == INTERVALTIMER_TIMER2){
        // return the address for timer 2
        return XPAR_AXI_TIMER_2_BASEADDR;
    }
    // An invalid timer number was given
    else{
        // return INTERVALTIMER_FAILURE status instead of a good address
        return INTERVALTIMER_FAILURE;
    }
}

// read both sections of a timer and return the two
// number concatenated together as a 64 bit value
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint64_t getTimerValue(uint32_t timerNumber){
    // Decode timerNumber and get an address for the timer
    uint32_t addr = getTimerAddress(timerNumber);
    // store the lower counter value
    uint32_t lower = Xil_In32(addr + INTERVALTIMER_TCR0);
    // store the upper counter value as a 64 bit number
    uint64_t total = Xil_In32(addr + INTERVALTIMER_TCR1);

    // shift the upper value into the upper INTERVALTIMER_MAX_TIMER_VAL2 bits of the 64 bit number
    total = total << INTERVALTIMER_SHIFT32;
    // add the lower value into the lower INTERVALTIMER_MAX_TIMER_VAL2 bits of the 64 bit number
    total += lower;
    // return the two number as one nicely packed 64 bit number
    return total;
}

// set only one bit in any register
// address: the address of the register that contains the bit to be set
// value: which bit in the register you wish to set
void setBit(uint32_t address, uint32_t value){
    // read the value that's currently in the address
    uint32_t reg = Xil_In32(address);
    // do a bitwise or to ensure the bit gets set.
    // leave it alone if it's already set.
    reg = reg | value;
    // write the new register value to the register
    Xil_Out32(address, reg);
}

// clear only one bit in any register
// address: the address of the register that contains the bit to be set
// value: which bit in the register you wish to set
void clearBit(uint32_t address, uint32_t value){
    // read the value that's currently in the address
    uint32_t reg = Xil_In32(address);
    // do a bitwise and with the inverse of the bit to be cleared
    reg = reg & ~value;
    // write the new value to the register
    Xil_Out32(address, reg);
}

// start the timer
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint32_t intervalTimer_start(uint32_t timerNumber){
    // decode the timerNumber to an address
    uint32_t addr = getTimerAddress(timerNumber);
    // set the increment bit in the counter register
    setBit(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_ENT0);
    // if nothing went wrong, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}

// stop a timer
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint32_t intervalTimer_stop(uint32_t timerNumber){
    // decode the timerNumber to an address
    uint32_t addr = getTimerAddress(timerNumber);
    // clear the increment bit in the counter register
    clearBit(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_ENT0);
    // if nothing went wrong, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}

// reset the value in the counter back to 0
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint32_t intervalTimer_reset(uint32_t timerNumber){
    // decode the timerNumber to an address
    uint32_t addr = getTimerAddress(timerNumber);

    // clear the load register
    Xil_Out32(addr + INTERVALTIMER_TLR0, INTERVALTIMER_CLEAR);
    // load the value set into the counter
    setBit(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_LOAD0);
    // stop loading into the register
    clearBit(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_LOAD0);

    // repeat the previous steps for the other half of the timer
    Xil_Out32(addr + INTERVALTIMER_TLR0, INTERVALTIMER_CLEAR);
    setBit(addr + INTERVALTIMER_TCSR1, INTERVALTIMER_LOAD0);
    clearBit(addr + INTERVALTIMER_TCSR1, INTERVALTIMER_LOAD0);

    // if nothing went wrong, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}

// set up the timer as cascaded and reset
// timerNumber: a number representing the timer you want. 0, 1, 2 are valid values.
uint32_t intervalTimer_init(uint32_t timerNumber){
    // decode the timerNumber to an address
    uint32_t addr = getTimerAddress(timerNumber);
    // if we got a valid address back from getTimerAddress:
    if(addr != 0){
        // set INTERVALTIMER_TCSR0 to 0
        Xil_Out32(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_CLEAR);
        // Set the cascade bit in INTERVALTIMER_TCSR0
        setBit(addr + INTERVALTIMER_TCSR0, INTERVALTIMER_CASC);
        // set the other half of the counter to 0
        Xil_Out32(addr + INTERVALTIMER_TCSR1, INTERVALTIMER_CLEAR);
        // if nothing went wrong, return INTERVALTIMER_SUCCESS
        return INTERVALTIMER_SUCCESS;
    }
    else{
        // an invalid timerNumber was given, return INTERVALTIMER_FAILURE
        return INTERVALTIMER_FAILURE;
    }
}

// initialize all three counter blocks
uint32_t intervalTimer_initAll(){
    // for counters 0, 1, and 2 do:
    for(uint32_t i = 0; i < INTERVALTIMER_MAX_TIMER_VAL; i++){
        // initialize the current counter (i)
        intervalTimer_init(i);
    }
    // if nothing went wrong, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}

// reset all three of the counter blocks back to 0
uint32_t intervalTimer_resetAll(){
    // for counters 0, 1, and 2 do:
    for(uint32_t i = 0; i < INTERVALTIMER_MAX_TIMER_VAL; i++){
        // initialize the current counter (i)
        intervalTimer_reset(i);
    }
    // if nothing went wrong, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}

// this function will make sure a timer works by veryfing the following functionality:
// reset: makes sure the value goes to 0 when reset
// start: start the counter and make sure the value is changing
// stop: stop the counter and make sure the value is not changing
uint32_t intervalTimer_runTest(uint32_t timerNumber){
    // initialize the timer so it's ready to use
    intervalTimer_init(timerNumber);
    // reset the timer
    intervalTimer_reset(timerNumber);
    // make sure the value is 0
    if(getTimerValue(timerNumber)!= 0){
        // if it's not 0, tell me
        printf("Counters not reset correctly.\n");
    }

    // start the timer
    intervalTimer_start(timerNumber);
    // set up a variable to store the counter value in
    uint32_t time = 0;

    // read the value on the counter INTERVALTIMER_MAX_TIMER_VAL times and compare it to the
    // previously read value to make sure it's different
    for(int i = 0; i < INTERVALTIMER_MAX_TIMER_VAL; i++){
        // if the the old time and new time are the same:
        if(time == getTimerValue(timerNumber)){
            // tell me about it!
            printf("Counter did not increase!\n");
        }
        // get a new value for the counter
        time = getTimerValue(timerNumber);
    }

    // stop the timer
    intervalTimer_stop(timerNumber);

    // get the value of the counter now that it's stopped
    time = getTimerValue(timerNumber);

    // read the value on the counter INTERVALTIMER_MAX_TIMER_VAL times and compare it to the
    // previously read value to make sure they're equal
    for(int i = 0; i < INTERVALTIMER_MAX_TIMER_VAL; i++){
        // if the previous value is not equal to the current value:
        if(time != getTimerValue(timerNumber)){
            // tell me about it!
            printf("Counter increased\n");
        }
        // get a new timer value
        time = getTimerValue(timerNumber);
    }

    // return success whether something worked or not
    return INTERVALTIMER_SUCCESS;
}


// Run intervalTimer_runTest for all INTERVALTIMER_MAX_TIMER_VAL timers.
// Return INTERVALTIMER_SUCCESS if nothing went wrong.
uint32_t intervalTimer_testAll(){
    // cycle through all of the timers.
    for(uint32_t i = 0; i < INTERVALTIMER_MAX_TIMER_VAL; i++){
        // run the test for the current counter (i)
        intervalTimer_runTest(i);
    }
    // Return INTERVALTIMER_SUCCESS if nothing went wrong.
    return INTERVALTIMER_SUCCESS;
}

// This function reads the total value in the counters
// and returns it in seconds.
// Relies on getTimerValue
// timerNumber: The timer that you want to read. 0, 1, or 2 are valid inputs.
// seconds: a variable in which to store the duration.
uint32_t intervalTimer_getTotalDurationInSeconds(uint32_t timerNumber, double *seconds){
    // Set seconds equal to the total counter value divided by the clock frequency
    *seconds = getTimerValue(timerNumber) / INTERVALTIMER_CLOCK_FREQ;
    // If we got this far, return INTERVALTIMER_SUCCESS
    return INTERVALTIMER_SUCCESS;
}
