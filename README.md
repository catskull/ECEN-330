# ECEN-330
My lab code for BYU's ECEN 330 class

This is C code compiled for the ARM core of the Digilent ZYBO dev board. More information on the class can be found [here](http://ecen330wiki.groups.et.byu.net/wiki/doku.php?id=start).

![ZYBO dev board](http://i.imgur.com/B9gRGFq.jpg)
